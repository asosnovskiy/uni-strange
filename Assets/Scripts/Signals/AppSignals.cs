﻿using strange.extensions.signal.impl;
using System;

public class AppStartSignal : Signal { }
public class AppQuitSignal : Signal { }
public class AppPauseSignal : Signal<bool> { }

public class HardwareBackPressSignal : Signal { }