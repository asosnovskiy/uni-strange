﻿using strange.extensions.context.impl;
using strange.extensions.injector.api;
using strange.extensions.signal.impl;
using UnityEngine;

public class MainContextView : ContextView
{
    [SerializeField]
    private UIManager _uiManager;

	private MainContext _context;

    void Start()
    {
		_context = new MainContext(_uiManager, this);

		_context.Start();

		_context.Launch();
    }

    //Dispose all objects then app die
    void OnApplicationQuit()
    {
        DispatchSignal<AppQuitSignal>();
    }

    void OnApplicationPause(bool paused)
    {
		DispatchSignal<AppPauseSignal, bool>(paused);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Backspace))
        {
            DispatchSignal<HardwareBackPressSignal>();
        }
    }

    private void DispatchSignal<T>() where T : Signal
    {
		if(_context == null)
			return;
		
		var signal = _context.GetComponent<T>() as T;

        if (signal == null)
            return;

        signal.Dispatch();
    }

	private void DispatchSignal<T,V>(V val) where T : Signal<V>
	{
		if(_context == null)
			return;

		var signal = _context.GetComponent<T>() as T;

		if (signal == null)
			return;

		signal.Dispatch(val);
	}
}