public abstract class BaseState : IState
{
	[Inject]
	public IStateMachine StateMachine { get; private set; }

	[Inject]
	public HardwareBackPressSignal HardwareBackPressSignal { get; private set; }

	public virtual void Load()
	{
		HardwareBackPressSignal.AddListener(OnHardwareBackPress);
	}

	public virtual void Unload()
	{
		HardwareBackPressSignal.RemoveListener(OnHardwareBackPress);
	}

	protected virtual void OnHardwareBackPress()
	{
		if(StateMachine.LastState != this)
			return;

		StateMachine.Unload(false);
	}
}