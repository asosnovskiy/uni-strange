﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Collections;

public class MainConfig
{
	public static string PackageName
    {
        get
        {
			return Application.bundleIdentifier;
        }
    }
}