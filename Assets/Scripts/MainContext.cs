using System;
using strange.extensions.command.impl;
using UnityEngine;

public class MainContext : SignalContext
{
    private readonly UIManager _uiManager;

    public MainContext(UIManager uiManager, MonoBehaviour view)
        : base(view)
    {
        _uiManager = uiManager;
    }

    protected override void mapBindings()
    {
        base.mapBindings();

        injectionBinder.Bind<MainConfig>().ToSingleton();
        injectionBinder.Bind<MainModel>().ToSingleton();
		injectionBinder.Bind<IStateMachine>().To<StateMachine>().ToSingleton();

        BindStates();
        BindViews();

        //signals and command
        commandBinder.Bind<AppQuitSignal>();
        commandBinder.Bind<AppStartSignal>()
            .InSequence()
            .To<AppStartCommand>()
            .Once();

        commandBinder.Bind<HardwareBackPressSignal>();
    }

    private void BindStates()
    {
	}

    private void BindViews()
    {
        if (injectionBinder == null)
            throw new Exception();

        if (_uiManager == null)
            throw new Exception();
		
    }
}
