﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.IO;

public class CreateScriptsHelpers : EditorWindow
{
	[MenuItem ("Window/StrangeHelper")]
	public static void  ShowWindow ()
	{
		EditorWindow.GetWindow (typeof(CreateScriptsHelpers));
	}

	private string _injections = "";
	private string _newBaseStateName = "";
	private string _newPopupName = "";

	private const string CSharp_Extension = ".cs";
	private static readonly string StatesPath = "Scripts/States/";
	private static readonly string CommandsPath = "Scripts/Commands/";
	private static readonly string ViewsPath = "Scripts/Views/";
	private static readonly string UIManagerFile = "Scripts/UIManager.cs";
	private static readonly string MainContextFile = "Scripts/MainContext.cs";

	void OnGUI ()
	{
		GUILayout.Label ("Base Settings", EditorStyles.boldLabel);
		_injections = EditorGUILayout.TextField ("Injections", _injections);
		GUILayout.BeginHorizontal ();
		_newBaseStateName = EditorGUILayout.TextField ("BaseStateName", _newBaseStateName);

		if (CanCreateState(_newBaseStateName) && GUILayout.Button ("Create", GUILayout.Width (100))){
			if (!_newBaseStateName.EndsWith ("State"))
				_newBaseStateName += "State";
				
			string fullPath = Path.Combine (Path.Combine(Application.dataPath,StatesPath), _newBaseStateName + CSharp_Extension);

			File.WriteAllText(fullPath, GetBaseStateString(_newBaseStateName, _injections.Split(new char[]{','}, System.StringSplitOptions.RemoveEmptyEntries)));

			WriteStateToMainContext(_newBaseStateName);
		}

		GUILayout.EndHorizontal ();

		GUILayout.BeginHorizontal ();
		_newPopupName = EditorGUILayout.TextField ("NewPopup", _newPopupName);

		if (CanCreateView(_newPopupName) && GUILayout.Button ("Create", GUILayout.Width (100))){
			//1. create file
			string fullPath = Path.Combine (Path.Combine(Application.dataPath,ViewsPath), _newPopupName + CSharp_Extension);

			File.WriteAllText(fullPath, GetBasePopupString(_newPopupName, _injections.Split(new char[]{','}, System.StringSplitOptions.RemoveEmptyEntries)));

			//2. add to UIManager
			var uiManagerStrings = File.ReadAllLines(Path.Combine(Application.dataPath,UIManagerFile));

			var uiManagerList = new List<string>();

			foreach (var line in uiManagerStrings) {
				if(line.Trim() != "}"){
					uiManagerList.Add(line);
				}else{
					uiManagerList.Add(@"	public " + _newPopupName + " " + _newPopupName + ";");
					uiManagerList.Add(line);
					break;
				}
			}

			File.WriteAllLines(Path.Combine(Application.dataPath,UIManagerFile), uiManagerList.ToArray());

			//3. add to MainContext
			WriteUIToMainContext(_newPopupName);
		}

		GUILayout.EndHorizontal ();

//		GUILayout.BeginHorizontal ();
//		_newPopupName = EditorGUILayout.TextField ("NewLoadCmd", _newPopupName);
//
//		if (CanCreateView(_newPopupName) && GUILayout.Button ("Create", GUILayout.Width (100))){
//			//1. create file
//			string fullPath = Path.Combine (Path.Combine(Application.dataPath,ViewsPath), _newPopupName + CSharp_Extension);
//
//			File.WriteAllText(fullPath, GetBasePopupString(_newPopupName, _injections.Split(new char[]{','}, System.StringSplitOptions.RemoveEmptyEntries)));
//
//			//2. add to UIManager
//			var uiManagerStrings = File.ReadAllLines(Path.Combine(Application.dataPath,UIManagerFile));
//
//			var uiManagerList = new List<string>();
//
//			foreach (var line in uiManagerStrings) {
//				if(line.Trim() != "}"){
//					uiManagerList.Add(line);
//				}else{
//					uiManagerList.Add(@"	public " + _newPopupName + " " + _newPopupName + ";");
//					uiManagerList.Add(line);
//					break;
//				}
//			}
//
//			File.WriteAllLines(Path.Combine(Application.dataPath,UIManagerFile), uiManagerList.ToArray());
//
//			//3. add to MainContext
//			WriteUIToMainContext(_newPopupName);
//		}
//
//		GUILayout.EndHorizontal ();

		if(GUILayout.Button ("Update Assets", GUILayout.Width (200))){
			AssetDatabase.Refresh();
		}
	}

	private bool CanCreateState(string name)
	{
		if (name.Length == 0) {
			GUILayout.Label("Enter name..", GUILayout.Width(100));
			return false;
		}

		if (!name.EndsWith ("State"))
			name += "State";

		if (File.Exists (Path.Combine (Application.dataPath, StatesPath + name + CSharp_Extension))) {
			GUILayout.Label("Already yet!", GUILayout.Width(100));
			return false;
		}

		return true;
	}

	private bool CanCreateView(string name)
	{
		if (string.IsNullOrEmpty(name)) {
			GUILayout.Label("Enter name..", GUILayout.Width(100));
			return false;
		}

		if (File.Exists (Path.Combine (Application.dataPath, ViewsPath + name + CSharp_Extension))) {
			GUILayout.Label("Already yet!", GUILayout.Width(100));
			return false;
		}

		return true;
	}

	private string GetBaseStateString(string name, string[] injections = null)
	{
		var injStr = "";

		if(injections != null && injections.Length > 0){
			injStr += "\r\n";

			for (int i = 0; i < injections.Length; i++) {
				var injName = injections[i].Trim();

				injStr += GetInjectString(injName) + "\r\n";
			}
		}

		return @"public class {0} : BaseState
{{1}
	public override void Load ()
	{
		base.Load ();
	}

	public override void Unload ()
	{
		base.Unload ();
	}
}".Replace("{0}",name).Replace("{1}", injStr);
	}

	private string GetBasePopupString(string name, string[] injections = null)
	{
		var injStr = "";

		if(injections != null && injections.Length > 0){
			injStr += "\r\n";

			for (int i = 0; i < injections.Length; i++) {
				var injName = injections[i].Trim();

				injStr += GetInjectString(injName) + "\r\n";
			}
		}

		return @"public class {0} : BasePopup
{{1}
	
}".Replace("{0}",name).Replace("{1}", injStr);
	}

	private string GetInjectString(string name)
	{
		if(name == "|")
			return @"	";
		
		return @"	[Inject]
	public {0} {0} { get; private set; }".Replace("{0}",name);
	}

	private void WriteUIToMainContext(string name)
	{
		WriteToMainContext("private void BindViews()",@"		injectionBinder.Bind<{0}>().ToValue(_uiManager.{0}).ToSingleton();".Replace("{0}", name));
	}

	private void WriteStateToMainContext(string name)
	{
		WriteToMainContext("private void BindStates()",@"		injectionBinder.Bind<{0}>().To<{0}>();".Replace("{0}", name));
	}

	private void WriteToMainContext(string when, string what)
	{
		var contextStrings = File.ReadAllLines(Path.Combine(Application.dataPath,MainContextFile));

		var list = new List<string>();

		bool foundedfunc = false;
		bool added = false;

		foreach (var line in contextStrings) {

			if(line.Trim() == when){
				foundedfunc = true;
			}

			if(foundedfunc && !added && line.Trim() == "}")
			{
				list.Add(what);
				added = true;
			}

			list.Add(line);
		}

		File.WriteAllLines(Path.Combine(Application.dataPath,MainContextFile), list.ToArray());
	}
}